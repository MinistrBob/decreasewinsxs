﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace DecreaseWinsxs
{
    public partial class Form1 : Form
    {
        private DataSet1 _ds;
        private DataTable _listFiles;
        public Form1()
        {
            this._ds = new DataSet1();
            this._listFiles = _ds.ListFiles;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
//            The size on disk should be the sum of the size of the clusters that store the file:
//  long sizeondisk = clustersize * ((filelength + clustersize - 1) / clustersize);

//You'll need to dip into P/Invoke to find the cluster size; GetDiskFreeSpace() returns it.
            // http://www.pinvoke.net/default.aspx/kernel32/GetDiskFreeSpace.html

            //ds = dataSet1;
            //ds.Clear();
            


            // Получим список файлов
            // 
            this.Cursor = Cursors.WaitCursor;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var txtFiles = Directory.EnumerateFiles(@"c:\", "*", SearchOption.AllDirectories);

            try
            {
                foreach (FileInfo folder in txtFiles.Select(x => new FileInfo(x)))
                {
                    DataRow row1 = _listFiles.NewRow();
                    row1["FullName"] = folder.FullName;
                    row1["FileName"] = folder.Name;
                    row1["Lenght"] = folder.Length;
                    _listFiles.Rows.Add(row1);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show(ex.Message);
            }
            sw.Stop();
            this.Cursor = Cursors.Default;

            label1.Text = sw.Elapsed.ToString();
            label2.Text = _listFiles.Rows.Count.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            foreach (DataRow row1 in _listFiles.Rows)
            {
                listBox1.Items.Add(row1["FullName"] + ";" + row1["FileName"] + ";" + row1["Lenght"]);
            }
        }
    }
}
